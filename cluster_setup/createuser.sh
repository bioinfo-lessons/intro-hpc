# script to automate the creation of a user in the cluster
# passwordless ssh, slurm access
# should be run from the parent directory of the intro-hpc repo

if [ "$#" -ne 1 ]
then
    echo "Usage: $0 <userid>"
    exit 1
fi

user=$1

useradd --create-home $user --shell /usr/bin/bash
echo $user:mbioinfoisciii | chpasswd
mkdir -p /home/$user/.ssh
chmod 700 /home/$user/.ssh
cat intro-hpc/cluster_setup/ssh/intro-hpc.pub > /home/$user/.ssh/authorized_keys
chmod 640 /home/$user/.ssh/authorized_keys
chown -R $user:$user /home/$user/.ssh
sacctmgr -i add account users
sacctmgr -i create user name=$user account=users
