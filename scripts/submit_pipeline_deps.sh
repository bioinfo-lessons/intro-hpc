pid_cutadapt=$(sbatch -J cutadapt -o log/cutadapt.out -e log/cutadapt.err -c 1 --mem=1G -t1 --wrap "bash scripts/cutadapt.sh" | cut -d" " -f4)
pid_star=$(sbatch --dependency=afterok:$pid_cutadapt -J star -o log/star.out -e log/star.err -c 1 --mem=1G -t1 --wrap "bash scripts/star.sh" | cut -d" " -f4)
sbatch --dependency=afterok:$pid_star -J multiqc -o log/multiqc.out -e log/multiqc.err -c 1 --mem=1G -t1 --wrap "bash scripts/multiqc.sh"
