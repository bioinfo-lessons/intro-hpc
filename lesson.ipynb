{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Introduction to High Performance Computing\n",
    "\n",
    "This lesson will introduce some concepts on High Performance Computing.\n",
    "\n",
    "You will be working on a cluster of computers located in the cloud. These computers will implement a queuing system to coordinate the execution of jobs.\n",
    "\n",
    "Lesson formats:\n",
    "- [git repository](https://gitlab.com/bioinfo-lessons/intro-hpc)\n",
    "- [slides](https://bioinfo-lessons.gitlab.io/intro-hpc/)\n",
    "- [HTML](https://bioinfo-lessons.gitlab.io/intro-hpc/notebook.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## High Performance Computing, The Cloud, and other buzzwords\n",
    "\n",
    "The words \"high-performance computing\", \"cluster\", and \"cloud\" have become popular \"buzzwords\" during the past few years, and they are used in different contexts with varying degrees of coherence and correctness."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "A \"cluster\" of computers is a group of interconnected computers that tipically share a common purpose, and work in a coordinated fashion to fullfill certain tasks, which would be too demanding for a single, standard computer.\n",
    "<br/><br/>\n",
    "<img src=\"img/clusters.jpg\" width=\"850\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "An HPC system (also referred to as a \"supercomputer\") is, quite simply, [\"a computer with a high level of performance compared to a general-purpose computer\"](https://en.wikipedia.org/wiki/Supercomputer). HPC systems are more often than not clusters of computers, rather than a single super-machine.\n",
    "\n",
    "<img src=\"img/mare_nostrum.jpg\" />"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The \"cloud\" is a generic term commonly used to refer to a group of computing resources physically located in a remote location.\n",
    "\n",
    "<img src=\"img/cloud_computing.png\" width=\"500\"/>\n",
    "<div align=\"right\"><font size=\"1\"><a href=\"https://commons.wikimedia.org/w/index.php?curid=6080417\" target=\"_blank\">Image by By Sam Johnston</a></font></div>\n",
    "\n",
    "A \"cloud\" can provide any kind of computational resources: data processing servers, storage servers, web servers, e-mail servers, etc. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Connecting to a remote resource to get work done is far from being a novel concept: computers used to be big, expensive pieces of machinery, and users would use \"dumb\" terminals to connect to them and get work done.\n",
    "\n",
    "Eventually, technology reached a point where we could have a desktop computer that was powerful enough to perform complicated tasks, and the concept of connecting to a remote, more powerful computer died out.\n",
    "\n",
    "Nowadays, single computers struggle to cope with the deluge of data that's generated in all fields of work."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Thanks to the hiper-connected nature of the world, it is now trivial to access remote resources independently of their location.\n",
    "\n",
    "This has given rise to the current concept of \"cloud computing\" and \"cloud services\".\n",
    "\n",
    "Many services that you may commonly use depend on some type of cloud infrastructure: Youtube, Netflix, Amazon, Google, and so on."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "We can now easily access large amounts of computing resources by simply renting them from one of many providers (Google Cloud, Amazon Web Services, OpenStack providers).\n",
    "\n",
    "Large computers or even clusters of them can be created and shut down on demand, while payment is only made for usage time."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Job schedulers\n",
    "\n",
    "A job scheduler (or queueing system, workflow manager, batch scheduler, etc.), is a program that manages the background execution of jobs.\n",
    "\n",
    "In this lesson we'll be using [Slurm](https://slurm.schedmd.com/), a free and open source job scheduler that is used in many large systems worldwide."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Job scheduler workflow\n",
    "<br/><br/>\n",
    "<img src=\"img/queueing_workflow.png\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Accessing the test cluster\n",
    "\n",
    "In order to access the cluster, we need to configure ssh to use a key file that the cluster recognises (provided by your instructor).\n",
    "\n",
    "In order to make the remote access easier, add these lines to your ~/.ssh/config file.\n",
    "\n",
    "```shell\n",
    "Host cluster\n",
    "    Hostname <ip_address_of_login_node> #provided by the instructor\n",
    "    IdentityFile ~/.ssh/advshell_rsa\n",
    "    User <my_assigned user> #assigned by the instructor\n",
    "```\n",
    "\n",
    "Then obtain the ssh key:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "--2020-11-10 00:58:27--  https://gitlab.com/bioinfo-lessons/intro-hpc/-/raw/master/cluster_setup/ssh/intro-hpc\n",
      "Resolving gitlab.com (gitlab.com)... 172.65.251.78, 2606:4700:90:0:f22e:fbec:5bed:a9b9\n",
      "Connecting to gitlab.com (gitlab.com)|172.65.251.78|:443... connected.\n",
      "HTTP request sent, awaiting response... 200 OK\n",
      "Length: unspecified [text/plain]\n",
      "Saving to: ‘intro-hpc’\n",
      "\n",
      "intro-hpc               [ <=>                ]   2.54K  --.-KB/s    in 0s      \n",
      "\n",
      "2020-11-10 00:58:28 (13.8 MB/s) - ‘intro-hpc’ saved [2602]\n",
      "\n"
     ]
    }
   ],
   "source": [
    "mkdir -p ~/.ssh\n",
    "cd ~/.ssh\n",
    "wget https://gitlab.com/bioinfo-lessons/intro-hpc/-/raw/master/cluster_setup/ssh/intro-hpc\n",
    "\n",
    "chmod 600 ~/.ssh/advshell_rsa #the private key must only be readable by us"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "After this, you can login into the cluster."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ssh cluster"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Understanding the cluster\n",
    "\n",
    "Let's check what resources we have available. Let's start by seeing what kind of machine the login node is.\n",
    "\n",
    "First, let's see how many and what kind of CPUs we have."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Architecture:          x86_64\n",
      "CPU op-mode(s):        32-bit, 64-bit\n",
      "Byte Order:            Little Endian\n",
      "CPU(s):                2\n",
      "On-line CPU(s) list:   0,1\n",
      "Thread(s) per core:    2\n",
      "Core(s) per socket:    1\n",
      "Socket(s):             1\n",
      "NUMA node(s):          1\n",
      "Vendor ID:             GenuineIntel\n",
      "CPU family:            6\n",
      "Model:                 63\n",
      "Model name:            Intel(R) Xeon(R) CPU @ 2.30GHz\n",
      "Stepping:              0\n",
      "CPU MHz:               2300.000\n",
      "BogoMIPS:              4600.00\n",
      "Hypervisor vendor:     KVM\n",
      "Virtualization type:   full\n",
      "L1d cache:             32K\n",
      "L1i cache:             32K\n",
      "L2 cache:              256K\n",
      "L3 cache:              46080K\n",
      "NUMA node0 CPU(s):     0,1\n",
      "Flags:                 fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss ht syscall nx pdpe1gb rdtscp lm constant_tsc rep_good nopl xtopology nonstop_tsc pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt aes xsave avx f16c rdrand hypervisor lahf_lm abm invpcid_single ssbd ibrs ibpb stibp kaiser fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid xsaveopt arat md_clear arch_capabilities\n"
     ]
    }
   ],
   "source": [
    "lscpu"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Let's now look at the memory available."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "              total        used        free      shared  buff/cache   available\n",
      "Mem:           7.3G        203M        5.0G         10M        2.1G        6.8G\n",
      "Swap:            0B          0B          0B\n"
     ]
    }
   ],
   "source": [
    "free -h"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "So the login node we've accessed has two CPUs and a total ~8GB of RAM memory. Not super-powerful, but enough for its job as a login node.\n",
    "\n",
    "Let's now run a Slurm commands to check what kind of cluster we are accessing."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "PARTITION AVAIL  TIMELIMIT  NODES  STATE NODELIST\n",
      "main*        up   infinite      4  idle compute-large[001-002],compute-small[001-002]\n",
      "small        up   infinite      2  idle compute-small[001-002]\n",
      "large        up   infinite      2  idle compute-large[001-002]\n"
     ]
    },
    {
     "ename": "",
     "evalue": "127",
     "output_type": "error",
     "traceback": []
    }
   ],
   "source": [
    "sinfo"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This shows us a good deal of information about the topology of the cluster:\n",
    "\n",
    "- There are three partitions, \"main\", \"small\", and \"large\".\n",
    "- There are four nodes, compute-small001 and 002, and compute-large001 and 002.\n",
    "- The main queue uses all nodes, while small and large use the small and large nodes correspondingly\n",
    "- The queues are not running any jobs (\"idle\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "We now know the login node characteristics, and understand the topology of the cluster.\n",
    "\n",
    "Let's now understand the kind of nodes we have available. We'll start with the small nodes. First, let's ssh into one of them:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ssh compute-small001"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once inside it, let's do the same CPU and RAM checks we did on the login node."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Architecture:          x86_64\n",
      "CPU op-mode(s):        32-bit, 64-bit\n",
      "Byte Order:            Little Endian\n",
      "CPU(s):                8\n",
      "On-line CPU(s) list:   0-7\n",
      "Thread(s) per core:    2\n",
      "Core(s) per socket:    4\n",
      "Socket(s):             1\n",
      "NUMA node(s):          1\n",
      "Vendor ID:             GenuineIntel\n",
      "CPU family:            6\n",
      "Model:                 63\n",
      "Model name:            Intel(R) Xeon(R) CPU @ 2.30GHz\n",
      "Stepping:              0\n",
      "CPU MHz:               2300.000\n",
      "BogoMIPS:              4600.00\n",
      "Hypervisor vendor:     KVM\n",
      "Virtualization type:   full\n",
      "L1d cache:             32K\n",
      "L1i cache:             32K\n",
      "L2 cache:              256K\n",
      "L3 cache:              46080K\n",
      "NUMA node0 CPU(s):     0-7\n",
      "Flags:                 fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss ht syscall nx pdpe1gb rdtscp lm constant_tsc rep_good nopl xtopology nonstop_tsc pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt aes xsave avx f16c rdrand hypervisor lahf_lm abm invpcid_single ssbd ibrs ibpb stibp kaiser fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid xsaveopt arat md_clear arch_capabilities:\n"
     ]
    }
   ],
   "source": [
    "lscpu"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "And now the RAM."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "              total        used        free      shared  buff/cache   available\n",
      "Mem:           7.0G        189M        5.2G         18M        1.6G        6.5G\n",
      "Swap:            0B          0B          0B\n"
     ]
    }
   ],
   "source": [
    "free -h"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Seems like these \"small\" machines are already more powerful than our login node, featuring 8 CPUs each, and ~8GB of RAM.\n",
    "\n",
    "Now let's see what the \"large\" machines look like."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Architecture:          x86_64\n",
      "CPU op-mode(s):        32-bit, 64-bit\n",
      "Byte Order:            Little Endian\n",
      "CPU(s):                16\n",
      "On-line CPU(s) list:   0-15\n",
      "Thread(s) per core:    2\n",
      "Core(s) per socket:    8\n",
      "Socket(s):             1\n",
      "NUMA node(s):          1\n",
      "Vendor ID:             GenuineIntel\n",
      "CPU family:            6\n",
      "Model:                 63\n",
      "Model name:            Intel(R) Xeon(R) CPU @ 2.30GHz\n",
      "Stepping:              0\n",
      "CPU MHz:               2300.000\n",
      "BogoMIPS:              4600.00\n",
      "Hypervisor vendor:     KVM\n",
      "Virtualization type:   full\n",
      "L1d cache:             32K\n",
      "L1i cache:             32K\n",
      "L2 cache:              256K\n",
      "L3 cache:              46080K\n",
      "NUMA node0 CPU(s):     0-15\n",
      "Flags:                 fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss ht syscall nx pdpe1gb rdtscp lm constant_tsc rep_good nopl xtopology nonstop_tsc pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt aes xsave avx f16c rdrand hypervisor lahf_lm abm invpcid_single ssbd ibrs ibpb stibp kaiser fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid xsaveopt arat md_clear arch_capabilities\n"
     ]
    }
   ],
   "source": [
    "exit #exit the small node first\n",
    "ssh compute-large002\n",
    "lscpu"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "              total        used        free      shared  buff/cache   available\n",
      "Mem:            14G        270M         12G         18M        1.6G         13G\n",
      "Swap:            0B          0B          0B\n"
     ]
    }
   ],
   "source": [
    "free -h"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These \"large\" machines have 16 CPUs each, and ~16GB of RAM.\n",
    "\n",
    "So it seems like we are dealing with a 4-node cluster, where two nodes have 8 CPUs and 8GB RAM, and two nodes have 16 CPUs and 16GB RAM. This means that there are 48 total CPUs available, and there is 1GB of RAM available per CPU, which is a reasonable -if not staggering- amount of memory."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Using the queue\n",
    "\n",
    "Wielding the power of information about the cluster that we're accessing, we can now start using the queue.\n",
    "\n",
    "Let's start by checking the currently running jobs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)\n"
     ]
    },
    {
     "ename": "",
     "evalue": "127",
     "output_type": "error",
     "traceback": []
    }
   ],
   "source": [
    "squeue"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Apparently there's nothing running right now, so we just get the column headers as output."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "To use the queue, we will be using few scripts available in the \"scripts\" directory of the lesson git repository\n",
    "\n",
    "These scripts will simulate running some processes, but really all they are doing is waiting for a few seconds and informing the time at which they finish.\n",
    "\n",
    "Let's first clone the git repository, and then look at the directory contents."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Cloning into 'intro-hpc'...\n",
      "warning: redirecting to https://gitlab.com/bioinfo-lessons/intro-hpc.git/\n",
      "remote: Enumerating objects: 34, done.\u001b[K\n",
      "remote: Counting objects: 100% (34/34), done.\u001b[K\n",
      "remote: Compressing objects: 100% (33/33), done.\u001b[K\n",
      "remote: Total 73 (delta 9), reused 0 (delta 0), pack-reused 39\u001b[K\n",
      "Receiving objects: 100% (73/73), 2.38 MiB | 5.02 MiB/s, done.\n",
      "Resolving deltas: 100% (24/24), done.\n",
      "cutadapt.sh  multiqc.sh  star.sh  submit_pipeline_deps.sh  submit_pipeline.sh\n"
     ]
    }
   ],
   "source": [
    "git clone https://gitlab.com/bioinfo-lessons/intro-hpc\n",
    "ls intro-hpc/scripts"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "echo \"Running cutadapt...\"\n",
      "\n",
      "sleep 30 \n",
      "\n",
      "echo \"Done at $(date +\"%H:%M:%S\")!\"\n",
      "\n"
     ]
    }
   ],
   "source": [
    "cat intro-hpc/scripts/cutadapt.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The basic command to send jobs to the queue is the following:\n",
    "\n",
    "```shell\n",
    "sbatch -J <jobname> -o <logfile> -e <errfile> \\\n",
    "    -c <ncores> --mem=<total_memory>G --t<time_minutes> \\\n",
    "    --wrap \"<command>\"\n",
    "```\n",
    "\n",
    "- **jobname**: a name that allows us to identify our job in the queue\n",
    "- **logfile**: the path to a file where the stdout of the command will be saved\n",
    "- **errfile**: the path to a file where the stderr of the command will be saved\n",
    "- **ncores**: the number of cores we would like to ask the queue to assign to our job\n",
    "- **total_memory**: the amount of memory we would like to ask the queue to assign to our job\n",
    "- **time_minutes**: the estimated duration of our job\n",
    "- **command**: the command we would like to run"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "So let's try our fist submission to the queue."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 39,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Submitted batch job 18\n"
     ]
    },
    {
     "ename": "",
     "evalue": "127",
     "output_type": "error",
     "traceback": []
    }
   ],
   "source": [
    "mkdir log #somewhere to put our log files\n",
    "cd intro-hpc\n",
    "sbatch -J cutadapt -o log/cutadapt.out -e log/cutadapt.err -c 1 --mem=1G -t1 --wrap \"bash scripts/cutadapt.sh\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "And let's monitor it with squeue using the \"watch\" command, so it runs every 2 seconds."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Every 2.0s: squeue                                  login001: Thu Oct 24 15:01:50 2019\n",
      "             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)\n",
      "                17      main cutadapt    tdido  R       0:08      1 compute-small001\n"
     ]
    }
   ],
   "source": [
    "watch squeue"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "After the job disappears from the queue, let's check its output."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Running cutadapt...\n",
      "Done at 12:27:59!\n"
     ]
    }
   ],
   "source": [
    "cat log/cutadapt.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Great, cutadapt ran correctly on the cluster."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Exercise\n",
    "\n",
    "We've just sent cutadapt to the queue asking for 1 CPU and 1GB of RAM. Try now to send it again, but this time ask for 20GB of RAM."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Everything seems to be working, but up to now we're not doing anything worth of a queuing system.\n",
    "\n",
    "Let's now create a new script to submit all three processes together."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "sbatch -J cutadapt -o log/cutadapt.out -e log/cutadapt.err -c 1 --mem=1G -t1 --wrap \"bash scripts/cutadapt.sh\"\n",
      "sbatch -J star -o log/star.out -e log/star.err -c 1 --mem=1G -t1 --wrap \"bash scripts/star.sh\"\n",
      "sbatch -J multiqc -o log/multiqc.out -e log/multiqc.err -c 1 --mem=1G -t1 --wrap \"bash scripts/multiqc.sh\"\n"
     ]
    }
   ],
   "source": [
    "cat scripts/submit_pipeline.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "And let's submit them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Submitted batch job 101\n",
      "Submitted batch job 102\n",
      "Submitted batch job 103\n"
     ]
    },
    {
     "ename": "",
     "evalue": "127",
     "output_type": "error",
     "traceback": []
    }
   ],
   "source": [
    "bash scripts/submit_pipeline.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Reflect on what you just did. Does it make any sense?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "It doesn't: there are dependencies here, and STAR should wait for cutadapt, and multiqc should wait for STAR. Right now multiqc is even finishing before the rest:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Running cutadapt...\n",
      "Done at 13:44:46!\n",
      "Running multiqc...\n",
      "Done at 13:44:31!\n",
      "Running STAR...\n",
      "Done at 13:45:16!\n"
     ]
    }
   ],
   "source": [
    "cat log/*.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Let's modify the pipeline script to introduce dependencies between the jobs, so that slurm waits before executing a job that depends on another.\n",
    "\n",
    "The idea is to first capture the job id from the sbatch output and then pass it as an argument to the depending submission. Here's a simplified example with two jobs:\n",
    "\n",
    "```shell\n",
    "pid_first_job=$(sbatch ... --wrap \"first_job.sh\" | cut -d\" \" -f4)\n",
    "sbatch --dependency=afterok:$pid_first_job ... --wrap \"dependant_job.sh\"\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "pid_cutadapt=$(sbatch -J cutadapt -o log/cutadapt.out -e log/cutadapt.err -c 1 --mem=1G -t1 --wrap \"bash scripts/cutadapt.sh\" | cut -d\" \" -f4)\n",
      "pid_star=$(sbatch --dependency=afterok:$pid_cutadapt -J star -o log/star.out -e log/star.err -c 1 --mem=1G -t1 --wrap \"bash scripts/star.sh\" | cut -d\" \" -f4)\n",
      "sbatch --dependency=afterok:$pid_star -J multiqc -o log/multiqc.out -e log/multiqc.err -c 1 --mem=1G -t1 --wrap \"bash scripts/multiqc.sh\"\n"
     ]
    }
   ],
   "source": [
    "cat scripts/submit_pipeline_deps.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Let's now run it and watch as our jobs wait patiently in order to run.\n",
    "\n",
    "Notice that we only see one line of output because we are processing the first two to extract the process ids."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Submitted batch job 104\n"
     ]
    },
    {
     "ename": "",
     "evalue": "127",
     "output_type": "error",
     "traceback": []
    }
   ],
   "source": [
    "bash scripts/submit_pipeline_deps.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Send the pipeline with dependencies to the queue 20 times. See what kind of things happen in the queue."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Elasticluster: quick setup of cloud-based workflow managers\n",
    "\n",
    "The elasticluster software allows you to quickly set up a full-blown cloud-based workflow manager by deploying a cluster on any of a variety of compatible services.\n",
    "\n",
    "You can configure the cluster to be as large or small as you like, and start it and stop it on demand.\n",
    "\n",
    "It's a great tool to simplify the process of leveraging cloud resources on demand to satisfy the requirements of your projects.\n",
    "\n",
    "The course git repository contains the config files and instructions to set up the cluster used during the lesson in the Google cloud. You can see the [elasticluster documentation](https://elasticluster.readthedocs.io/en/latest/#) to adapt it to other providers."
   ]
  }
 ],
 "metadata": {
  "authors": [
   {
    "name": "Tomás Di Domenico"
   }
  ],
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  },
  "title": "Introduction to High Performance Computing"
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
